<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>talugo.com</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    
    <link href="../css/datepicker.css" rel="stylesheet">
    <link href="../css/estilo.css" rel="stylesheet">
    
    <!-- Favicon -->
    <link HREF="../img/favicon.ico" mce_HREF="../img/favicon.ico" REL='icon'>
    <link HREF="../img/favicon.ico" mce_HREF="../img/favicon.ico" REL='shortcut icon'>
    
    <!-- Fontes -->
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
	body{margin:10px;}
    </style>
	
  </head>
  <body>
  	
 <div class="text-center">
<h1>Produtos talugo</h1>
</div>
<form class="form-group" action="" method="GET">
	<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-2">
		<label>Buscar:</label>
		<input type="text" value="<?php echo $_GET['busca']; ?>" class="form-control" name="busca">
	</div>
	<div id="quanto_class" class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<button type="submit" class=" btn btn-success">PESQUISAR</button>
	</div>
</form>
<table class="table table-striped table-bordered" border=1 width=100%>
	<tr>
		<td>Foto</td>
		<td>Titulo</td>
		<td>Descricao</td>
		<td>Preço/Dia</td>
		<td>Talugador</td>
	</tr>
<?php

include("../classe/conexao.php");

$produto = "";

$sql_code = "SELECT p.pro_nome, p.pro_descricao, f.fot_arquivo, f.fot_codigo, p.pro_precodia, p.pro_codigo, e.elo_nome, e.elo_email, e.elo_telefone
	FROM produto p, pro_foto f, eloja e
	WHERE p.pro_nome LIKE '%$produto%'
	AND f.fot_pro_codigo = p.pro_codigo
	AND e.elo_codigo = p.pro_elo_codigo LIMIT 0";

if(isset($_GET['busca']) && strlen($_GET['busca']) > 0){
	$produto = $mysqli->escape_string($_GET['busca']);
	$sql_code = "SELECT p.pro_nome, p.pro_descricao, f.fot_arquivo, f.fot_codigo, p.pro_precodia, p.pro_codigo, e.elo_nome, e.elo_email, e.elo_telefone
	FROM produto p, pro_foto f, eloja e
	WHERE p.pro_nome LIKE '%$produto%'
	AND f.fot_pro_codigo = p.pro_codigo
	AND e.elo_codigo = p.pro_elo_codigo";
}



$sql_query = $mysqli->query($sql_code);
$dado = $sql_query->fetch_assoc();
$total = $sql_query->num_rows;

if($total > 0)
	do{
	?>

		<tr>
			<td><a target="_blank" href="../eloja/pro/<?php echo $dado[pro_codigo]; ?>/<?php echo $dado[fot_arquivo]; ?>"><img width="100" src="../eloja/pro/<?php echo $dado[pro_codigo]; ?>/<?php echo $dado[fot_arquivo]; ?>"></a></td>
			<td><?php echo $dado[pro_nome]; ?></td>
			<td><?php echo $dado[pro_descricao]; ?></td>
			<td><?php echo $dado[pro_precodia]; ?></td>
			<td><?php echo $dado[elo_nome]; ?> <br>
				<?php echo $dado[elo_email]; ?><br>

				<?php echo substr($dado[elo_telefone], 0,2). " ".substr($dado[elo_telefone], 2); ?></td>
		</tr>

	<?php
	} while($dado = $sql_query->fetch_assoc());
	else{
		?>
		<tr>
			<td colspan="5"><br>
				<p class="text-center	">Nenhum produto encontrado. Tente buscar usando apenas uma palavra.</p>
			</td>

		</tr>
		<?php
	}
	?>
</table>
</div>
</body>